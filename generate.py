#! /usr/bin/python3
import sys, re, random, argparse, pprint

#########################
# TILE GENERATION SCRIPT
#########################

# Basic usage
#   python3 generate.py # Generate a single table
#   python3 generate.py -n $(seq -s ',' 4) # Generate four tables named 1, 2, 3 and 4

##############################################
# DATA PARAMETERS
##############################################


### CITIES ###################################

# Data tables for common destination
# Each city is mapped to a pair (footprint (in ton CO2e), frequency of visit)
footprint_table = {'ABU DHABI': (1.2, 5.0),
 'ATLANTA': (1.6, 1.0),
 'NAPLES': (0.32, 4.0),
 'BANGALORE': (1.8, 2.0),
 'BARCELONA': (0.2, 16.0),
 'BEIJING': (1.8, 3.0),
 'BOSTON': (1.2, 8.0),
 'BERLIN': (0.16, 8.0),
 'BUENOS AIRES': (2.5, 4.0),
 'DALLAS': (1.8, 10.0),
 'DELHI': (1.5, 8.0),
 'DETROIT': (1.4, 9.0),
 'DUBAI': (1.2, 4.0),
 'DUBLIN': (0.2, 5.0),
 'GENEVA': (0.2, 7.0),
 'HO CHI MINH CITY': (2.3, 2.0),
 'HONG KONG': (2.1, 3.0),
 'LIMA': (2.3, 1.0),
 'LOS ANGELES': (2.0, 12.0),
 'MINNEAPOLIS': (1.5, 3.0),
 'MONTREAL': (1.2, 12.0),
 'MUMBAI': (1.6, 4.0),
 'NEW YORK': (1.3, 9.0),
 'NEWARK': (1.3, 1.0),
 'SALT LAKE CITY': (1.8, 5.0),
 'SAN FRANCISCO': (2.0, 10.0),
 'SANTIAGO': (2.6, 4.0),
 'STOCKHOLM': (0.4, 10.0),
 'HELSINKI': (0.4, 1.0),
 'PRAGUE': (0.3, 2.0),
 'VIENNE': (0.35, 2.0),
 'PORTO': (0.17, 2.0),
 'SAO PAULO': (2.1, 4.0),
 'SEATTLE': (1.8, 3.0),
 'SINGAPORE': (2.4, 5.0),
 'TAIPEI': (2.2, 10.0),
 'TEL AVIV': (0.7, 3.0),
 'TOKYO': (2.2, 7.0),
 'TORONTO': (1.3, 6.0),
 'VANCOUVER': (1.8, 13.0),
 'WASHINGTON DC': (1.4, 9.0),
 'NICE': (0.2, 22.0),
 'SYDNEY': (3.6, 4.0)
}

### Description of the fictive team
### Each index is a status, mapped to an object describing the status
### - Its members (name, CSS class for rendering)
### - Its total footprint over the period
### - Its total number of mission over the period
team = {
    'PhD': { 'members': [
        {'name':'PhD #1', 'class': 'phd1'},
        {'name':'PhD #2', 'class': 'phd2'},
        {'name':'PhD #3', 'class': 'phd3'},
        {'name':'PhD #4', 'class': 'phd4'}],
             'footprint': 7.8,
             'number': 12},
    'rangA': { 'members': [{'name':'Prof/DR', 'class': 'dr'}],
               'footprint': 7.1,
               'number': 9},
    'technique': { 'members': [{'name':'Ingénieur·e', 'class': 'inge'}],
                   'footprint': 2.0,
                   'number': 3},
    'rangB': { 'members': [{'name':'McF/CR #1', 'class': 'cr1'}, {'name':'McF/CR #2', 'class': 'cr2'}],
               'footprint': 6.4,
               'number': 9},
    'postdoc': { 'members': [{'name':'Postdoc', 'class': 'postdoc'}],
                 'footprint': 1.62,
                 'number': 3 }
}


### Some predicates for motives
def is_permanent(m):
    return m['rank'] in ['rangA', 'rangB']

def is_cr(m):
    return m['rank'] in ['rangB']

def is_dr(m):
    return m['rank'] in ['rangA']

def is_temp(m):
    return not (m['rank'] in ['rangA', 'rangB'])
def is_phd(m):
    return (m['rank'] in ['PhD'])

def far(m):
    return m['footprint'] >= 1.

### List of reasons for traveling
reason_table = [
    ["Conférence annuelle A* (sans exposé)", 4],
    ["Conférence annuelle A* (avec exposé)", 4],
    ["Conférence annuelle A (sans exposé)", 3],
    ["Conférence annuelle A (avec exposé)", 3],
    ["Conférence annuelle B (sans exposé)", 2],
    ["Conférence annuelle B (avec exposé)", 2],
    ["Participation à un workshop", 3],
    [["Keynote dans LA conférence", is_permanent], 2],
    [["Accompagner le doctorant à sa première conf", is_permanent], 3],
    ["Participation à un workshop", 5],
    [["Jury soutenance de thèse", is_dr], 3],
    [["Jury soutenance de thèse", is_cr], 1],
    [["Exposé invité", is_permanent], 3],
    [["Jury de sélection", is_dr], 3],
    [["Jury de sélection", is_cr], 1],
    [["Visite de travail d'un mois", far], 1],
    [["Visite de travail de six mois", far], 1],
    [["Visite du labo en co-tutelle", lambda m: far(m) and is_phd(m)], 1],
    [["Réunion de consortium d'un projet", is_permanent], 2],
    [["Réunion de montage de projet", is_permanent], 2],
    [["École d'été d'une semaine", is_phd], 4]
]


# We can now compute the main parameter, the "cm per ton"
# i.e. how much CO² does one centimeter of the width of a tile represent?
# The constraints is that the biggest tile needs to be representable
total_co2 = sum ([team[status]['footprint'] for status in team])

def rank_of_person(person):
    """Given the name of a person, look up its rank in the team description"""
    for x in team:
        if person in team[x]['members']:
            return x
    return 'unknown: ' +person

mission_id = 0
def make_id():
    """Generate a fresh mission ID"""
    global mission_id
    mission_id += 1
    return mission_id


def make_mission (person, city, motive):
    """Create a mission from person, city, motive, by filling up the other fields"""
    return { 'person': person,
             'city': city,
             'footprint': footprint_table[city.upper()][0],
             'id': make_id(),
             'motive': motive,
             'rank': rank_of_person(person) }

def html_of_mission(mission, cm_per_ton, width, tile_height):
    """HTML format a mission"""
    div_class = mission['rank']
    length = cm_per_ton * mission['footprint']
    if length < 1:
        div_class += ' verysmall'
    elif length < 6:
        div_class += ' small'
    elif length < 8:
        div_class += ' big'
    else:
        div_class += ' verybig big'
    if mission['table_name']:
        table = f"""<span class="table-name"><span>{mission['table_name']}</span></span>"""
    else:
        table = ""
        
    return f"""<div style="--tile-height: {tile_height}cm; --length: {length}cm" class="outer-tuile {div_class}">
               <div class="tuile-inner {div_class}">
               <span class="header">
                  <span class="where">{mission['city']}</span>
                  <span class="person {mission["person"]["class"]}">{mission['person']['name']}</span>
               </span>
               <span class="descr">{mission['motive']}</span>
               {table}
               <span class="reverse">
                  <span class="where">{mission['city']}</span>
                  <span class="person {mission["person"]["class"]}">{mission['person']['name']}</span>
               </span>
    
               </div></div>"""
#    return f"""<div style="--tile-height: {tile_height}cm; --length: {length}cm" class="outer-tuile {div_class}">
#    </div>"""
        

### Generation
def draw(list):
    """Draw an element from a weighted list"""
    return random.choices( population=[l[0] for l in list], weights=[l[1] for l in list], k=1)[0]

def draw_motive(person, city):
    """Draw a motive consistent with person and city. Return the corresponding mission."""
    while True:
        # Draw a motive from the table
        motive = draw(reason_table)
        # motives can either be strings (i.e. motive applicable to all contexts)
        # or pairs [motive, f] where f is a predicate
        if type(motive) == str:
            return make_mission(person, city, motive)
        else:
            mission = make_mission(person, city, motive[0])
            # Check the built mission against the predicate
            if motive[1](mission):
                return mission

def draw_mission(rank, allowed_footprint):
    """Draw a mission for the given rank, within the allowed remaining footprint"""
    person = draw ([ [x, 1.] for x in team[rank]['members'] ])
    cities = [ [city, frequency] for city, (footprint, frequency) in footprint_table.items() if footprint < allowed_footprint]
    city = draw(cities)
    mission = draw_motive(person, city)
    return mission

def generate_missions_for_rank(rank, table_name=None):
    """Generate all the missions for a given rank"""
    missions = []
    error = 100
    # Loop until we find a drawing that is within 5% in terms of emissions
    while error > 0.05:
        missions = []
        target_footprint = team[rank]['footprint']
        footprint_left = target_footprint
        for i in range(team[rank]['number']):
            m = draw_mission(rank, max(footprint_left, 0.4))
            m['table_name'] = table_name
            missions.append(m)
            footprint_left -= m['footprint']
        error = abs(footprint_left) / target_footprint
    return missions

def generate_missions(table_name=None):
    """Generate the missions for the team."""
    missions = []
    for rank in team:
        missions += generate_missions_for_rank(rank, table_name)
    return missions

def pave_missions(input_missions, line_width, cm_per_co2):
    """Reorder missinons to ensure a good paving when exported to html.

    The input is a list of missions, the output is a list of list of missions : i.e. a list of lines.

    line_width is the width in cm of the paper to print the tile on.
    cm_per_co2 is the scale
    """
    lines = []
    missions = list(input_missions)
    biggest_tile = max(missions, key = lambda m: m['footprint'])

    co2_per_line = line_width / cm_per_co2
    current_line_co2 = 0.
    current_line_tiles = []
    while missions != []:
        # How much CO2 we  can fit on the current line
        candidates = [ m for m in missions if m['footprint'] <= co2_per_line - current_line_co2]

        # All tiles are too big to fit on the current line
        # And the current line is not empty
        if candidates == [] and current_line_co2 > 0.:
            lines.append(current_line_tiles)
            current_line_co2 = 0.
            current_line_tiles = []
            continue
        elif candidates == [] and current_line_co2 == 0.:
            print ('***Should not happen')
        else:
            # Add the largest fitting mission
            m = max(candidates, key = lambda m: m['footprint'])
            missions.remove(m)
            current_line_tiles.append(m)
            current_line_co2 += m['footprint']

    # At the end, don't forget to flush current line
    if current_line_tiles != []:
        lines.append(current_line_tiles)
    return lines

def pave_missions_by_lines(input_missions, line_number):
    """Pave missions on a number of lines"""

    lines = [ [] for i in range(line_number) ]
    while input_missions != []:
        m = max(input_missions, key = lambda m: m['footprint'])
        input_missions.remove(m)
        # Find shorter line
        min = sum([x['footprint'] for x in lines[0]])
        index = 0
        for i in range(len(lines)-1):
            s = sum([x['footprint'] for x in lines[i+1]])
            if s < min:
                min = s
                index = i+1
        lines[index].append(m)
    return lines
        
                
        
### Parsing from a file
def missions(mission_file):
    """Parse the mission of a file"""
    handle = open(mission_file, "r")
    lines = handle.readlines()
    handle.close()
    missions = []
    dict = {}
    for line in lines:
        result = re.search(r"(.*): *(.*)", line)
        if result:
            dict[result[1]] = result[2]
        if line == '---\n':
            missions.append(make_mission(dict['person'], dict['city'], dict['motive']))
            dict = {}
    if dict != {}:
        missions.append(make_mission(dict['person'], dict['city'], dict['motive']))
    return missions

def total_emissions_by_status(missions):
    """Compute the total emisions by rank"""
    result = {rank: 0 for rank in team}
    for m in missions:
        result[m["rank"]] += m["footprint"]
    return result

def max_column_width (lines):
    return max ([sum([m['footprint'] for m in line]) for line in lines])

def output_missions(lines, width, height, cm_per_ton, handle):
    """visual: do we want to create a pretty picture?"""
    handle.write(
        """
    <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css" />
        </head>
        <body>""")

    jetons_class = "jetons"
    handle.write(f"""
    <div style="--jetons-width: {width}cm; --jetons-height:{height}cm" class="{jetons_class}">
    """)
    for line in lines:
        for mission in line:
            handle.write(html_of_mission(mission, cm_per_ton, width, height))
        handle.write('<br />\n')
    handle.write("</div>")
    handle.write("\n</body>\n</html>")
    

parser = argparse.ArgumentParser( prog=__file__, formatter_class=argparse.RawDescriptionHelpFormatter, description="""Generate tiles for the dd-rennes workshop""")
parser.add_argument('-n', '--number', help='Number (or list of numbers) of tables to generate.', default='1')
parser.add_argument('-o', '--output', help='Output file.', default='tuiles.html')

args = parser.parse_args()

table_names = [ str(i) for i in range(int(args.number)) ]

# Parameters of the table
line_number = 5.
line_width = 54.
line_height = 38.

tile_height = line_height / line_number

co2_per_line = total_co2 / line_number
cm_per_co2 = line_width / co2_per_line 


# Parameters of the paper to print on
print_line_width = 29 # A3

missions = []
for table_number in table_names:
    missions += generate_missions(table_name=table_number)

with open(args.output, 'w') as f:
    lines = pave_missions(missions, print_line_width, cm_per_co2)
    output_missions(lines, print_line_width, tile_height, cm_per_co2, f)

print(f'Wrote {args.output}')
